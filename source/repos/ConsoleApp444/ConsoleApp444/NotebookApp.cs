﻿using System;
using System.Collections.Generic;

namespace ConsoleApp444
{
    class Program
    {
        static void Main(string[] args)
        {
            var book = new Dictionary<int, Notebook>();
            while (true)
            {
                Console.WriteLine("Press 1 to create a new note");
                Console.WriteLine("Press 2 to edit an existing one");
                Console.WriteLine("Press 3 to delete a note");
                Console.WriteLine("Press 4 to see a particular note");
                Console.WriteLine("Press 5 to see all the notes with short info");
                Console.WriteLine("Available variables: firstName, lastName, patronym, phoneNumber, country, birthDate, organisation, position, otherInfo");
                string response = Console.ReadLine();
                switch (response)
                {
                    case "1":
                        Console.WriteLine("Enter info like this: field1 = XXX, field2 = XXX, ..., where XXX are corresponding values");
                        Record.CreateNewNote(Console.ReadLine(), ref book);
                        Console.WriteLine($"Done! (id = {book.Count})\n");
                        break;
                    case "2":
                        Console.WriteLine("Enter id of the note:");
                        int id1 = Convert.ToInt32(Console.ReadLine());
                        if (id1 >= book.Count)
                        {
                            Console.WriteLine("Error");
                            break;
                        }
                        Console.WriteLine("Enter info like this: field1 = XXX, field2 = XXX, ..., where XXX are corresponding values");
                        Notebook obj = book[id1];
                        Record.EditNote(Console.ReadLine(), ref obj);
                        Console.WriteLine("Done!\n");
                        break;
                    case "3":
                        Console.WriteLine("Enter id of a note you want to delete:");
                        int id2 = Convert.ToInt32(Console.ReadLine());
                        Record.DeleteNote(ref book, id2);
                        Console.WriteLine("Done!\n");
                        break;
                    case "4":
                        Console.WriteLine("Enter id of a note you want to show:");
                        int id3 = Convert.ToInt32(Console.ReadLine());
                        Record.ReadNote(ref book, id3);
                        Console.WriteLine("");
                        break;
                    case "5":
                        Record.ShowAllNotes(ref book);
                        Console.WriteLine("");
                        break;
                    default:
                        Console.WriteLine("Error. Let's try again");
                        break;
                }
            }
        }
    }

    public class Notebook
    {
        public string lastName;
        public string firstName;
        public string patronym;
        public string phoneNumber;
        public string country;
        public string birthDate;
        public string organisation;
        public string position;
        public string otherInfo;
        public int id;
    }

    public class Record
    {
        public static void CreateNewNote(string arg, ref Dictionary<int, Notebook> book)
        {
            Notebook obj = new Notebook();
            book.Add(book.Count, obj);
            EditNote(arg, ref obj);
        }

        public static void EditNote(string arg, ref Notebook obj)
        {
            string[] values = arg.Split(", ");
            var dic = new Dictionary<string, string>();
            for (int i = 0; i < values.Length; i++)
            {
                dic.Add(values[i].Split(" = ")[0], values[i].Split(" = ")[1]);
            }
            foreach (KeyValuePair<string, string> d in dic)
            {
                switch (d.Key)
                {
                    case "lastName":
                        obj.lastName = d.Value;
                        break;
                    case "firstName":
                        obj.firstName = d.Value;
                        break;
                    case "patronym":
                        obj.patronym = d.Value;
                        break;
                    case "phoneNumber":
                        obj.phoneNumber = d.Value;
                        break;
                    case "country":
                        obj.country = d.Value;
                        break;
                    case "birthDate":
                        obj.birthDate = d.Value;
                        break;
                    case "organisation":
                        obj.organisation = d.Value;
                        break;
                    case "position":
                        obj.position = d.Value;
                        break;
                    case "otherInfo":
                        obj.otherInfo = d.Value;
                        break;
                    default:
                        Console.WriteLine("Error");
                        break;
                }
            }
        }

        public static void DeleteNote(ref Dictionary<int, Notebook> book, int id)
        {
            book.Remove(id);
        }

        public static void ReadNote(ref Dictionary<int, Notebook> book, int id)
        {
            if (book[id].firstName != null) Console.WriteLine($"first name: {book[id].firstName}");
            if (book[id].lastName != null) Console.WriteLine($"last name: {book[id].lastName}");
            if (book[id].patronym != null) Console.WriteLine($"patronym: {book[id].patronym}");
            if (book[id].phoneNumber != null) Console.WriteLine($"phone number: {book[id].phoneNumber}");
            if (book[id].country != null) Console.WriteLine($"country: {book[id].country}");
            if (book[id].birthDate != null) Console.WriteLine($"birthdate: {book[id].birthDate}");
            if (book[id].organisation != null) Console.WriteLine($"organisation: {book[id].organisation}");
            if (book[id].position != null) Console.WriteLine($"position: {book[id].position}");
            if (book[id].otherInfo != null) Console.WriteLine($"other info: {book[id].otherInfo}");
        }

        public static void ShowAllNotes(ref Dictionary<int, Notebook> book)
        {
            foreach (KeyValuePair<int, Notebook> p in book)
            {
                Console.WriteLine($"last name: {p.Value.lastName}");
                Console.WriteLine($"first name: {p.Value.firstName}");
                Console.WriteLine($"phone number: {p.Value.phoneNumber}");
                Console.WriteLine("\n");
            }
        }
    }
}
